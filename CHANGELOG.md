# Change Log

All notable changes to the "lst-view" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [Unreleased]
- All Hello World things need to be removed

## [0.0.4] - 2022.06.28
### Added

- Its now possible to renumber the record in front

## [0.0.5] - 2022.06.28
### Fixed

- If there was a Block number without any following code the renumbering did not work

## [0.0.8] - 2022.09.27
### Fixed

- String was not correctly recognised 


## [0.3.9] - 2023.02.17
### Fixed

- line numbering now works for mixed number groups

### Added

- code completion for g-codes, section comment and if/then clause
- comparator checking

## [0.3.10] - 2023.02.23
### Fixed

- Comparator checking was applied to multiple lines instead of a single line. This was causing false results and is now fixed

## [0.3.11] - 2023.02.23
### Fixed

- Gif links were not working in the markdown

## [0.3.12] - 2023.03.01
### Fixed

- Line renumbering was renumbering random numbers

## [0.3.14] - 2023.03.01
### Fixed

- added support for the following file extensions: spf,mpf and lst
- CASE was added as keyword
- added IF/ENDIF shortcut

## [0.3.15] - 2023.03.01
### Fixed

- Bosch files were previously not identified

## [0.4.15] - 2023.03.01
### Added

- Highlighting for the line numbers

## [0.4.16] - 2023.03.02
### Fixed

- When renumbering line numbers the leading zeros got removed

## [0.4.17] - 2023.03.03
### Fixed

- When renumbering line numbers the ones without trailing space got ignored

## [0.4.18] - 2023.03.15
### Fixed

- Could not add line comment anymore. The cause was the missing ```language-configuration.json```

## [0.5.18] - 2023.03.16
### Added

- line renumbering will now renumber the whole document if no selection is made

## [0.6.18] - 2023.03.16
### Added

- there is now a setting which will increment the line number after a section comment

## [0.6.19] - 2023.03.16
### Added

- added logo

## [0.7.19] - 2024.06.07
### Added

- added formatting document

## [0.7.20] - 2024.06.10
### Added

- added formatting for while-loops

## [0.8.20] - 2024.06.12
### Added

- added hover provider for lst-headers

## [0.8.21] - 2024.06.12
### Added

- added 4 spaces instead of a tab for formatting

## [0.8.22] - 2024.06.13
### Fixed

- updated regex for formatting

## [0.8.23] - 2024.06.13
### Added
- new logo (generated with Copilot)

## [0.8.24] - 2024.06.13
### Removed

- removed old not-working diagnostic feature

## [0.8.25] - 2024.06.13
## Added

- documented feature for lst-header

## [0.8.26] - 2024.07.03
# Added

- renumbering command to renumber document with dynamically set starting line number and step for incrementing the line numbers

