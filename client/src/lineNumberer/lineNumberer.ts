import * as vscode from 'vscode';
import { integer } from 'vscode-languageclient';

/**
 * increments the line number by 'step'
 * @param step the steps in which the line numbers will be incremented
 */
export function incrementLineNumbersBy(step: number) {

	const extensionConfigurations = vscode.workspace.getConfiguration("gcode-helper");
	const doesLineNumberIncrementAfterSectionComment: boolean = extensionConfigurations.get("comments.sectionComments.incrementLineNumber");
	const sectionCommentIncrementsLineNumberByStep: integer = extensionConfigurations.get("comments.sectionComments.incrementLineNumberBy");

	const editor = vscode.window.activeTextEditor;
	if (editor) {
		const document = editor.document;
		const selection = editor.selection;
		const hastUserMadeSelection: boolean = selection.isEmpty || selection.isSingleLine ? false : true;

		let dummy: string;
		if (selection.isEmpty || selection.isSingleLine) {
			dummy = document.getText();
		} else {
			dummy = document.getText(selection);
		}

		/**
		 * resembles the text of the active document
		 * it's either the text of the whole document or just a selection depending on whether the user
		 * made a selection in the first place
		 */
		const documentText = dummy;
		const lines: string[] = documentText.split(/\r?\n/);
		//collection of all line numbers of the document which are preceded by a comment section
		const lineNumbersPrecededBySectionComment: string[] = getLineNumbersPrecededBySectionComment(documentText);

		let startLineNumber = -1;
		let lineNumberAsString: string;
		let lineNumberAsNumber: number;
		let oldLineNumberAsNumber: number;

		for (const index in lines) {
			lineNumberAsString = lines[index].match(/^[a-zA-Z]?\d+\b/)?.[0];

			if (lineNumberAsString !== undefined) {
				//do not change the value of the first line number
				if (startLineNumber === -1) {
					startLineNumber = 1;
					lineNumberAsNumber = parseInt(lineNumberAsString.match(/\d+/)?.[0]);
				}
				else {
					lineNumberAsNumber += step;

					if (doesLineNumberIncrementAfterSectionComment) {
						if (lineNumbersPrecededBySectionComment.includes(lineNumberAsString)) {
							lineNumberAsNumber += sectionCommentIncrementsLineNumberByStep;
						}
					}

					//TODO: simplify regex so we only have to replace the number and not the letter which might
					//or might not precede the line number
					oldLineNumberAsNumber = parseInt(lineNumberAsString.match(/\d+/)?.[0]);
					lineNumberAsString = lineNumberAsString.replace(oldLineNumberAsNumber.toString(), lineNumberAsNumber.toString());
					lines[index] = lines[index].replace(/^[a-zA-Z]?\d+\b/, lineNumberAsString);
				}
			}
		}

		const newDocumentText = lines.join('\n');

		/**
		 * this is where the text of the active document is replaced
		 * if @constant hastUserMadeSelection is equal true, only the selection is replaced otherwise the whole document
		 */
		editor.edit(editBuilder => {
			if (hastUserMadeSelection) {
				editBuilder.replace(selection, newDocumentText);
			} else {
				editBuilder.replace(new vscode.Range(document.lineAt(0).range.start, document.lineAt(document.lineCount - 1).range.end), newDocumentText);
			}
		});
	}
}

function getLineNumbersPrecededBySectionComment(documentText: string): string[] {
	const lineNumbersPrecededBySectionComment: string[] = [];
	const regex = /(?:;\*+$)(?:.)+?(?:;\*+$)(?:.)+?(?<lineNumber>^[a-zA-Z]?\d+)/msg;

	let match: RegExpExecArray;
	while ((match = regex.exec(documentText)) !== null) {
		lineNumbersPrecededBySectionComment.push(match.groups.lineNumber);
	}

	return lineNumbersPrecededBySectionComment;
}

export function renumber(start: number, step: number) {
	console.log(start);
	const editor = vscode.window.activeTextEditor;

	if (editor) {
		const doc = editor.document;
		const text = doc.getText();

		const regex = /^N[0-9]*(?<!\s)/m;
		const match = regex.exec(text);

		if (match) {
			const startPos = doc.positionAt(match.index);
			const endPos = doc.positionAt(match.index + match[0].length);
			const range = new vscode.Range(startPos, endPos);

			editor.edit(editBuilder => {
				editBuilder.replace(range, `N${start}`);
			}).then(() => {
				incrementLineNumbersBy(step);
			});
		}
	}
}