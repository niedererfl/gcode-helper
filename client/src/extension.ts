/* --------------------------------------------------------------------------------------------
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 * ------------------------------------------------------------------------------------------ */

import * as path from 'path';
import * as vscode from 'vscode';

import { HoverProvider } from './classes/HoverProvdier';

import {
	LanguageClient,
	LanguageClientOptions,
	ServerOptions,
	TransportKind
} from 'vscode-languageclient/node';
import { incrementLineNumbersBy, renumber } from './lineNumberer/lineNumberer';
import * as formatter from './fomatter/formatter';

let client: LanguageClient;

export function activate(context: vscode.ExtensionContext) {

	const renumber1 = vscode.commands.registerCommand('Renumber1', () => {

		const step = 1;

		incrementLineNumbersBy(step);
	});

	const renumber10 = vscode.commands.registerCommand('Renumber10', () => {

		const step = 10;

		incrementLineNumbersBy(step);
	});

	const renumber100 = vscode.commands.registerCommand('Renumber100', () => {

		const step = 100;

		incrementLineNumbersBy(step);
	});

	const renumber1000 = vscode.commands.registerCommand('Renumber1000', () => {

		const step = 1000;

		incrementLineNumbersBy(step);
	});


	vscode.languages.registerDocumentFormattingEditProvider('gcode', {
		provideDocumentFormattingEdits(document: vscode.TextDocument): vscode.TextEdit[] {
			let edits: vscode.TextEdit[] = [];
			const lineCount = document.lineCount;

			edits = formatter.format(edits, lineCount, document);

			return edits;
		}
	});

	vscode.languages.registerHoverProvider('gcode', new HoverProvider());

	const disposable = vscode.commands.registerCommand('extension.renumber', async () => {
        const start = await vscode.window.showInputBox({ prompt: 'Please enter the starting line number' });
        const step = await vscode.window.showInputBox({ prompt: 'Please enter the step for incrementing the line numbers' });

        if (start && step) {
            const startNumber = Number(start);
            const stepNumber = Number(step);

            if (isNaN(startNumber) || isNaN(stepNumber)) {
                vscode.window.showErrorMessage('Please enter valid numbers');
            } else {
                renumber(startNumber, stepNumber);
            }
        }
    });

    context.subscriptions.push(disposable);
	context.subscriptions.push(renumber1);
	context.subscriptions.push(renumber10);
	context.subscriptions.push(renumber100);
	context.subscriptions.push(renumber1000);

	context.subscriptions.push(vscode.commands.registerCommand("Renumber Step 1", () => renumber1));
	context.subscriptions.push(vscode.commands.registerCommand("Renumber Step 10", () => renumber10));
	context.subscriptions.push(vscode.commands.registerCommand("Renumber Step 100", () => renumber100));
	context.subscriptions.push(vscode.commands.registerCommand("Renumber Step 1000", () => renumber1000));

	// The server is implemented in node
	const serverModule = context.asAbsolutePath(
		path.join('server', 'out', 'server.js')
	);

	// If the extension is launched in debug mode then the debug server options are used
	// Otherwise the run options are used
	const serverOptions: ServerOptions = {
		run: { module: serverModule, transport: TransportKind.ipc },
		debug: {
			module: serverModule,
			transport: TransportKind.ipc,
		}
	};

	// Options to control the language client
	const clientOptions: LanguageClientOptions = {
		// Register the server for gcode and cpl documents
		documentSelector: [
			{ scheme: 'file', language: 'gcode' },
			{ scheme: 'file', language: 'cpl' }
		],
		synchronize: {
			// Notify the server about file changes to '.clientrc files contained in the workspace
			fileEvents: vscode.workspace.createFileSystemWatcher('**/.clientrc')
		}
	};

	// Create the language client and start the client.
	client = new LanguageClient(
		'languageServerExample',
		'Language Server Example',
		serverOptions,
		clientOptions
	);

	// Start the client. This will also launch the server
	client.start();
}

export function deactivate(): Thenable<void> | undefined {
	if (!client) {
		return undefined;
	}
	return client.stop();
}
