import * as vscode from 'vscode';

export function format(edits: vscode.TextEdit[], lineCount: number, document: vscode.TextDocument) {
	let spaceAfterLineNumber = "";

	for (let i = 0; i < lineCount; i++) {
		const line = document.lineAt(i);

		if (checkForClosingStatement(line)) {
			if (spaceAfterLineNumber.includes(" ".repeat(4))) {
				spaceAfterLineNumber = spaceAfterLineNumber.replace(" ".repeat(4), "");
			}
		}

		const edit: vscode.TextEdit = replaceSpaceAfterLineNumber(line, spaceAfterLineNumber);
		if (edit != null) {
			edits.push(edit);
		}

		if (checkForOpeningStatement(line)) {
			spaceAfterLineNumber = spaceAfterLineNumber + " ".repeat(4);
		}
	}

	return edits;
}

function checkForClosingStatement(line: vscode.TextLine): boolean {
	const closingStatementRegex = /^N\d*\s*((ENDIF)|(ELSE)|(ENDFOR)|(ENDWHILE))/;
	return closingStatementRegex.test(line.text);
}

function replaceSpaceAfterLineNumber(line: vscode.TextLine, spaceAfterLineNumber: string): vscode.TextEdit {
	let indentedText = "";

	const formattedText = formatTextOnLine(line.text);

	const match: RegExpMatchArray = formattedText.match(/^(N\d+)\s+(.*)/);
	if (match) {
		const lineNumber = match[1];
		const content = match[2];
		indentedText = `${lineNumber}${spaceAfterLineNumber + " "}${content}`;
	}
	else {
		return null;
	}

	const range = new vscode.Range(
		line.range.start,
		line.range.start.translate(0, line.text.length)
	);
	const textEdit = new vscode.TextEdit(range, indentedText);

	return textEdit;
}

function checkForOpeningStatement(line: vscode.TextLine): boolean {
	const openingStatementRegex = /^N\d*\s*((IF)|(FOR)|(WHILE)|(ELSE))/;
	const gotoStatementRegex = /\s+((GOTO))/;
	if (gotoStatementRegex.test(line.text)) {
		return false;
	}
	return openingStatementRegex.test(line.text);
}

function formatTextOnLine(text: string): string {
	const formattedText = text.replace(/\s+(IF)\s*(\()/, " IF (");
	return formattedText;
}