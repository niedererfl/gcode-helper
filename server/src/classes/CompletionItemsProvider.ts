import { CompletionItem, CompletionItemKind, InsertTextFormat } from 'vscode-languageserver';
import * as jsonCompletionItems from '../completion_items/completion_items.json';

export class CompletionItemsProvider {

	private _completionItems: CompletionItem[] = [];
	public get completionItems(): CompletionItem[] {
		if (this._completionItems.length == 0) {
			this.completionItems = this.getCompletionItems();
		}
		return this._completionItems;
	}
	public set completionItems(v: CompletionItem[]) {
		this._completionItems = v;
	}

	/**
	 * 
	 * @returns an array of CompletionItems
	 */
	private getCompletionItems(): CompletionItem[] {
		return (jsonCompletionItems as JsonCompletionItem[]).map(function (item: JsonCompletionItem) {
			const completionItem: CompletionItem =
			{
				detail: item.detail,
				documentation: typeof item.documentation == 'string' ? item.documentation : item.documentation.join(""),
				insertText: typeof item.insertText == 'string' ? item.insertText : item.insertText.join(""),
				insertTextFormat: InsertTextFormat.Snippet,
				kind: CompletionItemKind.Snippet,
				label: item.label
			};
			return completionItem;
		});
	}
}