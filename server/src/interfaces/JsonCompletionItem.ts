interface JsonCompletionItem {
	detail: string;
	documentation: string | string[];
	insertText: string | string[];
	label: string;
}