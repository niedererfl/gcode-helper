# Extension with support for g-code

At the moment the following file extensions are supported:
<ul>
    <li>.gcode</li>
    <li>.spf</li>
    <li>.mpf</li>
    <li>.lst</li>
</ul>

## Highlighting
The extension highlights diffrent patterns with diffrent colors.

![example of highlighting of the line numbers](resources/img/lineNuber_highlighting_example.png)

## Formatting document
With this extension formatting your gcode is possible. To format the document you can either right click and select `Format Document` or
you can also set a shortcut for it. Also you can set format on save.

![example](resources/gif/code_formatting_example.gif)

## Code definitions
This extension provides code definition for a small collection of g-code

![example](resources/gif/definitions_example.gif)

## Code completion
Additionally there is a support for a handful of shortcuts as well as a shortcut for section comments

![example](resources/gif/code_completion_example.gif)

## Renumbering
You have the option to renumber a whole document or just part of it as well.

### Renumber document with starting number
There is a function called renumber which allows you to renumber the whole document with dynamically set starting linenumber and step.

![example](resources/gif/renumbering_example.gif)

### Renumber selected part of a document
There is also a function which allows you to renumber line numbers of the selected part with 3 diffrent static steps.

![example](resources/gif/renumbering_selected_example.gif)
## Section comments
When using section comments it might be useful to increment the line numbers afterwards. This extension can do this for you.

To use this feature you first have to enable it in the settings.
For that simply search for these two settings and modify them:

<ul>
	<li>gcode-helper.comments.sectionComments.incrementLineNumber</li>
	<li>gcode-helper.comments.sectionComments.incrementLineNumberBy</li>
</ul>

![example](resources/gif/comment_section_line_renumbering_example.gif)

## LST-Header hovering
When hovering over values from a table it provides you with the definition string from the table

![example](resources/gif/hovering_provider_example.gif)