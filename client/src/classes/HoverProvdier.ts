import * as vscode from 'vscode';
import { doc } from '../test/helper';

export class HoverProvider implements vscode.HoverProvider {
    provideHover(
        document: vscode.TextDocument,
        position: vscode.Position,
        token: vscode.CancellationToken
    ): vscode.ProviderResult<vscode.Hover> {

        // get line of current position - done
        // go up until line with 'C' found and save it to variable upperCLine - done
        // go down until line with 'C' found and save it to variable lowerCLine - done
        // check if first line under upperCLine indicates current position is in valuepairs (valuepairs start with 'ZA,DA,\d*'. each Valuepair starts with a 'DA')
        // if so, foreach valuepair create an array with the position of each value
        // get the index of the current hover by checking the position with each position in the valuepair array
        // from the upperCLine go up until the next CLine, save it to variable secondUpperCLine
        // get content and pack all definitions into an array
        // get definition with index and return it

        let index = null;

        const currentLine: number = position.line;
        let upperCLine = 0;
        let lowerCLine = 0;
        let searchingLine: number = currentLine;

        let hasFoundUpperCLine = false;
        
        while (!hasFoundUpperCLine){
            searchingLine--;
            const searchingTextLine: string = document.lineAt(searchingLine).text;
            if (searchingTextLine == 'C'){
                upperCLine = searchingLine;
                hasFoundUpperCLine = true;
            }
        }
                
        searchingLine = currentLine;
        let hasFoundLowerCLine = false;

        while (!hasFoundLowerCLine){
            searchingLine++;
            const searchingTextLine: string = document.lineAt(searchingLine).text;
            if (searchingTextLine == 'C'){
                lowerCLine = searchingLine;
                hasFoundLowerCLine = true;
            }
        }

        if (document.lineAt(upperCLine + 1).text.match(/ZA,DA,\d*/)){
            const valuePositions: Array<string> = [];

            let valuePairStartLine = currentLine;
            let valuePairEndLine = lowerCLine;
            
            searchingLine = currentLine;
            while (searchingLine > upperCLine){
                if (document.lineAt(searchingLine).text.match(/^DA,/)){
                    valuePairStartLine = searchingLine;
                    break;
                }
                searchingLine--;
            }
            searchingLine = currentLine;
            while (searchingLine < lowerCLine - 1){
                searchingLine++;
                if (document.lineAt(searchingLine).text.match(/^DA,/)){
                    valuePairEndLine = searchingLine;
                    break;
                }
            }

            for (let lineNumber = valuePairStartLine; lineNumber < valuePairEndLine; lineNumber++) {
                const lineText = document.lineAt(lineNumber).text;
        
                for (let charIndex = 0; charIndex < lineText.length; charIndex++) {
                    if (lineText[charIndex] === ',') {
                        valuePositions.push(`${lineNumber},${charIndex}`);
                    }
                }
            }

            let dummy = false;
            
            valuePositions.forEach(valuePosition => {
                if (Number(valuePosition.split(',')[0]) == valuePairEndLine - 1){
                    dummy = true;
                }
                if (Number(valuePosition.split(',')[0]) == currentLine){
                    if (Number(valuePosition.split(',')[1]) < position.character){
                        index = valuePositions.findIndex(item => item == valuePosition);
                    }
                    else if (index == null){
                        const dummy: Array<string> = [];
                        valuePositions.forEach(element => {
                            if(Number(element.split(',')[0]) == currentLine - 1){
                                dummy.push(element);
                            }
                        });
                        index = valuePositions.findIndex(item => item == dummy[dummy.length - 1]);
                    }
                }
            });

            if (!dummy){
                if(currentLine + 1 == valuePairEndLine){
                    index = valuePositions.length - 1; 
                }
            }
        }

        if (index != null){
            let secondUpperCLine = 0;
            searchingLine = upperCLine;
            let hasFoundSecondUpperCLine = false;

            while (!hasFoundSecondUpperCLine){
                searchingLine--;
                const searchingTextLine: string = document.lineAt(searchingLine).text;
                if (searchingTextLine == 'C'){
                    secondUpperCLine = searchingLine;
                    hasFoundSecondUpperCLine = true;
                }
            }

            const definitions: Array<string> = [];

            while (secondUpperCLine < upperCLine){
                secondUpperCLine++;
                if (!document.lineAt(secondUpperCLine).text.match(/^ZA,MM,\d*/)){
                    const definitionMatch = document.lineAt(secondUpperCLine).text.match(/(?<=')[^\\',]*(?=')/);
                    if(definitionMatch){
                        definitions.push(definitionMatch[0]);
                    }
                }
            }

            return new vscode.Hover(definitions[index]);
        }

		return null;
    }
}